# Corrales hechos con simpy

import simpy

print(f'STARTING SIMULATION')
print(f'----------------------------------')

camada = 1
E_1 = []
E_2 = []
E_3 = []
E_4 = []
E_5 = []

class Criadero:
	def __init__(self,env):
		self.gesta = simpy.Container(env,init = 0)
		self.lactancia = simpy.Container(env,init = 0)
		self.destete = simpy.Container(env,init = 0)
		self.cria = simpy.Container(env,init = 0)
		self.final = simpy.Container(env,init = 0)

def Ciclo_Camada(env,criadero,delay):

		yield env.timeout(delay*21)

		yield criadero.gesta.put(camada)
		dias_in = 21
		yield env.timeout(dias_in)
		
		dias_gesta = 21
		yield env.timeout(dias_gesta)
		yield criadero.gesta.get(camada)
		yield criadero.lactancia.put(camada)		
		
		dias_lactancia = 21
		yield env.timeout(dias_lactancia)
		yield criadero.lactancia.get(camada)
		yield criadero.destete.put(camada)
		
		dias_destete = 36
		yield env.timeout(dias_destete)
		yield criadero.destete.get(camada)
		yield criadero.cria.put(camada)
		
		dias_cria = 42
		yield env.timeout(dias_cria)
		yield criadero.cria.get(camada)
		yield criadero.final.put(camada)
		
		dias_final = 71
		yield env.timeout(dias_final)
		yield criadero.final.get(camada)

def collect_data(env):
	while True: 
		E_1.append(criadero.gesta.level)
		E_2.append(criadero.lactancia.level)
		E_3.append(criadero.destete.level)
		E_4.append(criadero.cria.level)
		E_5.append(criadero.final.level)
		yield env.timeout(1)

env = simpy.Environment()
criadero = Criadero(env)

# No me veeeassss

e_2 = env.process(Ciclo_Camada(env,criadero,1))
e_3 = env.process(Ciclo_Camada(env,criadero,2))
e_4 = env.process(Ciclo_Camada(env,criadero,3))
e_5 = env.process(Ciclo_Camada(env,criadero,4))
e_6 = env.process(Ciclo_Camada(env,criadero,5))
e_7 = env.process(Ciclo_Camada(env,criadero,6))
e_8 = env.process(Ciclo_Camada(env,criadero,7))
e_9 = env.process(Ciclo_Camada(env,criadero,8))
e_10= env.process(Ciclo_Camada(env,criadero,9))
e_11= env.process(Ciclo_Camada(env,criadero,10))
e_12= env.process(Ciclo_Camada(env,criadero,11))
e_13= env.process(Ciclo_Camada(env,criadero,12))
e_14= env.process(Ciclo_Camada(env,criadero,13))
e_15= env.process(Ciclo_Camada(env,criadero,14))
e_16= env.process(Ciclo_Camada(env,criadero,15))
e_17= env.process(Ciclo_Camada(env,criadero,16))
e_18= env.process(Ciclo_Camada(env,criadero,17))
e_19= env.process(Ciclo_Camada(env,criadero,18))
e_20= env.process(Ciclo_Camada(env,criadero,19))
e_21= env.process(Ciclo_Camada(env,criadero,20))
e_22= env.process(Ciclo_Camada(env,criadero,21))
e_23= env.process(Ciclo_Camada(env,criadero,22))
e_24= env.process(Ciclo_Camada(env,criadero,23))
e_25= env.process(Ciclo_Camada(env,criadero,24))
e_26= env.process(Ciclo_Camada(env,criadero,25))
e_27= env.process(Ciclo_Camada(env,criadero,26))
e_28= env.process(Ciclo_Camada(env,criadero,27))
e_29= env.process(Ciclo_Camada(env,criadero,28))
e_30= env.process(Ciclo_Camada(env,criadero,29))
e_31= env.process(Ciclo_Camada(env,criadero,30))

collect = env.process(collect_data(env))

env.run(until = 41*21)

print("Gesta:",criadero.gesta.level,"cerdos")
print("Lactancia:",criadero.lactancia.level,"cerdos")
print("Destete:",criadero.destete.level,"cerdos")
print("Cria:",criadero.cria.level,"cerdos")
print("Final:",criadero.final.level,"cerdos")
print(f'----------------------------------')
print(f'SIMULATION COMPLETED')

### Plots

from matplotlib import pyplot as plt

fig,axs = plt.subplots(3)

axs[0].plot(E_1,'tab:orange')
axs[0].set_title("Gesta")

axs[1].plot(E_2,'tab:green')
axs[1].set_title("Lactancia")

axs[2].plot(E_3,'tab:red')
axs[2].set_title("Destete")

plt.subplots_adjust(wspace=1,hspace=1)


for ax in axs.flat:
    ax.set(xlabel='Dias', ylabel='Nro de Camadas')


plt.savefig("plots1_3.png")
plt.cla()
plt.clf()

fig,axs = plt.subplots(2)
axs[0].plot(E_4,'tab:purple')
axs[0].set_title("Cria")

axs[1].plot(E_5)
axs[1].set_title("Final")

for ax in axs.flat:
    ax.set(xlabel='Dias', ylabel='Nro de Camadas')

plt.subplots_adjust(wspace=1,hspace=1)

plt.savefig("plots4_5.png")

"""
plt.title("Cantidad de camadas en función del tiempo")
plt.xlabel("Dias")
plt.ylabel("Nro de camadas")
plt.legend(loc='best')
plt.grid()
plt.savefig("plot_gesta.png")
plt.show()
"""

"""
DIAS=[]
for i in range(0,len(E_1)):
	DIAS.append(i)

### Animación !

import numpy as np
from matplotlib.animation import FuncAnimation
 

fig = plt.figure(1)

#Figura para gesta
ax1 = fig.add_subplot(131,xlim= (0,len(E_1)+21), ylim=(0,8))
line, = ax1.plot([],[])
ax1.set_ylabel('Nro de camadas')

#Figura para lactancia
ax2 = fig.add_subplot(132,xlim= (0,len(E_1)+21), ylim=(0,8))
line, = ax1.plot([],[])
ax1.set_ylabel('Nro de camadas')
"""